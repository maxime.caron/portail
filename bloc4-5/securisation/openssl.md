

# Présentation de `openSSL`


## Protocole SSL

Le protocole SSL (Secure Socket Layer) a été développé par la
société Netscape Communications Corporation pour permettre aux
applications client/serveur de communiquer de façon sécurisée. TLS
(Transport Layer Security) est une évolution de SSL réalisée par
l'IETF.

La version 3 de SSL est utilisée par les navigateurs tels Netscape et
Microsoft Internet Explorer depuis leur version 4.

SSL est un protocole qui s'intercale entre TCP/IP et les
applications qui s'appuient sur TCP. Une session SSL se déroule en
deux temps :

1.  une phase de poignée de mains (handshake) durant laquelle le
    client et le serveur s'identifient, conviennent du système de
    chiffrement et d'une clé qu'ils utiliseront par la suite.
2.  la phase de communication proprement dite durant laquelle les
    données échangées sont compressées, chiffrées et signées.

L'identification durant la poignée de mains est assurée à l'aide de
certificats X509.


## `openSSL`

[`openSSL`](http://www.~openssl~.org) est une boîte à outils cryptographiques implémentant les
protocoles SSL et TLS qui offre

1.  une bibliothèque de programmation en C permettant de réaliser
    des applications client/serveur sécurisées s'appuyant sur
    SSL/TLS.
2.  une commande en ligne (`openssl`) permettant 
    -   la création de clés RSA, DSA (signature) ;
    -   la création de certificats X509 ;
    -   le calcul d'empreintes (MD5, SHA, RIPEMD160, &#x2026;) ;
    -   le chiffrement et déchiffrement (DES, IDEA, RC2, RC4,
        Blowfish, &#x2026;) ;
    -   la réalisation de tests de clients et serveurs SSL/TLS ;
    -   la signature et le chiffrement de courriers (S/MIME).

Pour connaître toutes les fonctionnalités de `openSSL` : `man
   openssl` (dans un terminal en ligne de commande).

La syntaxe générale de la commande `openssl` est

```bash
$ openssl <commande> <options>

```

(le `$` est le prompt du shell)

Dans le texte qui suit, les commandes invoquant `openssl` supposent
que cette commande est dans votre PATH.


# Chiffrement symétrique avec `openSSL`

`openssl` permet d'utiliser plusieurs algorithme de chiffrement
symétrique. Pour obtenir une liste des systèmes disponibles :

```bash
openssl enc -list

```

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">Supported</td>
<td class="org-left">ciphers:</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">-aes-128-cbc</td>
<td class="org-left">-aes-128-cfb</td>
<td class="org-left">-aes-128-cfb1</td>
</tr>


<tr>
<td class="org-left">-aes-128-cfb8</td>
<td class="org-left">-aes-128-ctr</td>
<td class="org-left">-aes-128-ecb</td>
</tr>


<tr>
<td class="org-left">-aes-128-ofb</td>
<td class="org-left">-aes-192-cbc</td>
<td class="org-left">-aes-192-cfb</td>
</tr>


<tr>
<td class="org-left">-aes-192-cfb1</td>
<td class="org-left">-aes-192-cfb8</td>
<td class="org-left">-aes-192-ctr</td>
</tr>


<tr>
<td class="org-left">-aes-192-ecb</td>
<td class="org-left">-aes-192-ofb</td>
<td class="org-left">-aes-256-cbc</td>
</tr>


<tr>
<td class="org-left">-aes-256-cfb</td>
<td class="org-left">-aes-256-cfb1</td>
<td class="org-left">-aes-256-cfb8</td>
</tr>


<tr>
<td class="org-left">-aes-256-ctr</td>
<td class="org-left">-aes-256-ecb</td>
<td class="org-left">-aes-256-ofb</td>
</tr>


<tr>
<td class="org-left">-aes128</td>
<td class="org-left">-aes128-wrap</td>
<td class="org-left">-aes192</td>
</tr>


<tr>
<td class="org-left">-aes192-wrap</td>
<td class="org-left">-aes256</td>
<td class="org-left">-aes256-wrap</td>
</tr>


<tr>
<td class="org-left">-aria-128-cbc</td>
<td class="org-left">-aria-128-cfb</td>
<td class="org-left">-aria-128-cfb1</td>
</tr>


<tr>
<td class="org-left">-aria-128-cfb8</td>
<td class="org-left">-aria-128-ctr</td>
<td class="org-left">-aria-128-ecb</td>
</tr>


<tr>
<td class="org-left">-aria-128-ofb</td>
<td class="org-left">-aria-192-cbc</td>
<td class="org-left">-aria-192-cfb</td>
</tr>


<tr>
<td class="org-left">-aria-192-cfb1</td>
<td class="org-left">-aria-192-cfb8</td>
<td class="org-left">-aria-192-ctr</td>
</tr>


<tr>
<td class="org-left">-aria-192-ecb</td>
<td class="org-left">-aria-192-ofb</td>
<td class="org-left">-aria-256-cbc</td>
</tr>


<tr>
<td class="org-left">-aria-256-cfb</td>
<td class="org-left">-aria-256-cfb1</td>
<td class="org-left">-aria-256-cfb8</td>
</tr>


<tr>
<td class="org-left">-aria-256-ctr</td>
<td class="org-left">-aria-256-ecb</td>
<td class="org-left">-aria-256-ofb</td>
</tr>


<tr>
<td class="org-left">-aria128</td>
<td class="org-left">-aria192</td>
<td class="org-left">-aria256</td>
</tr>


<tr>
<td class="org-left">-bf</td>
<td class="org-left">-bf-cbc</td>
<td class="org-left">-bf-cfb</td>
</tr>


<tr>
<td class="org-left">-bf-ecb</td>
<td class="org-left">-bf-ofb</td>
<td class="org-left">-blowfish</td>
</tr>


<tr>
<td class="org-left">-camellia-128-cbc</td>
<td class="org-left">-camellia-128-cfb</td>
<td class="org-left">-camellia-128-cfb1</td>
</tr>


<tr>
<td class="org-left">-camellia-128-cfb8</td>
<td class="org-left">-camellia-128-ctr</td>
<td class="org-left">-camellia-128-ecb</td>
</tr>


<tr>
<td class="org-left">-camellia-128-ofb</td>
<td class="org-left">-camellia-192-cbc</td>
<td class="org-left">-camellia-192-cfb</td>
</tr>


<tr>
<td class="org-left">-camellia-192-cfb1</td>
<td class="org-left">-camellia-192-cfb8</td>
<td class="org-left">-camellia-192-ctr</td>
</tr>


<tr>
<td class="org-left">-camellia-192-ecb</td>
<td class="org-left">-camellia-192-ofb</td>
<td class="org-left">-camellia-256-cbc</td>
</tr>


<tr>
<td class="org-left">-camellia-256-cfb</td>
<td class="org-left">-camellia-256-cfb1</td>
<td class="org-left">-camellia-256-cfb8</td>
</tr>


<tr>
<td class="org-left">-camellia-256-ctr</td>
<td class="org-left">-camellia-256-ecb</td>
<td class="org-left">-camellia-256-ofb</td>
</tr>


<tr>
<td class="org-left">-camellia128</td>
<td class="org-left">-camellia192</td>
<td class="org-left">-camellia256</td>
</tr>


<tr>
<td class="org-left">-cast</td>
<td class="org-left">-cast-cbc</td>
<td class="org-left">-cast5-cbc</td>
</tr>


<tr>
<td class="org-left">-cast5-cfb</td>
<td class="org-left">-cast5-ecb</td>
<td class="org-left">-cast5-ofb</td>
</tr>


<tr>
<td class="org-left">-chacha20</td>
<td class="org-left">-des</td>
<td class="org-left">-des-cbc</td>
</tr>


<tr>
<td class="org-left">-des-cfb</td>
<td class="org-left">-des-cfb1</td>
<td class="org-left">-des-cfb8</td>
</tr>


<tr>
<td class="org-left">-des-ecb</td>
<td class="org-left">-des-ede</td>
<td class="org-left">-des-ede-cbc</td>
</tr>


<tr>
<td class="org-left">-des-ede-cfb</td>
<td class="org-left">-des-ede-ecb</td>
<td class="org-left">-des-ede-ofb</td>
</tr>


<tr>
<td class="org-left">-des-ede3</td>
<td class="org-left">-des-ede3-cbc</td>
<td class="org-left">-des-ede3-cfb</td>
</tr>


<tr>
<td class="org-left">-des-ede3-cfb1</td>
<td class="org-left">-des-ede3-cfb8</td>
<td class="org-left">-des-ede3-ecb</td>
</tr>


<tr>
<td class="org-left">-des-ede3-ofb</td>
<td class="org-left">-des-ofb</td>
<td class="org-left">-des3</td>
</tr>


<tr>
<td class="org-left">-des3-wrap</td>
<td class="org-left">-desx</td>
<td class="org-left">-desx-cbc</td>
</tr>


<tr>
<td class="org-left">-id-aes128-wrap</td>
<td class="org-left">-id-aes128-wrap-pad</td>
<td class="org-left">-id-aes192-wrap</td>
</tr>


<tr>
<td class="org-left">-id-aes192-wrap-pad</td>
<td class="org-left">-id-aes256-wrap</td>
<td class="org-left">-id-aes256-wrap-pad</td>
</tr>


<tr>
<td class="org-left">-id-smime-alg-CMS3DESwrap</td>
<td class="org-left">-rc2</td>
<td class="org-left">-rc2-128</td>
</tr>


<tr>
<td class="org-left">-rc2-40</td>
<td class="org-left">-rc2-40-cbc</td>
<td class="org-left">-rc2-64</td>
</tr>


<tr>
<td class="org-left">-rc2-64-cbc</td>
<td class="org-left">-rc2-cbc</td>
<td class="org-left">-rc2-cfb</td>
</tr>


<tr>
<td class="org-left">-rc2-ecb</td>
<td class="org-left">-rc2-ofb</td>
<td class="org-left">-rc4</td>
</tr>


<tr>
<td class="org-left">-rc4-40</td>
<td class="org-left">-seed</td>
<td class="org-left">-seed-cbc</td>
</tr>


<tr>
<td class="org-left">-seed-cfb</td>
<td class="org-left">-seed-ecb</td>
<td class="org-left">-seed-ofb</td>
</tr>


<tr>
<td class="org-left">-sm4</td>
<td class="org-left">-sm4-cbc</td>
<td class="org-left">-sm4-cfb</td>
</tr>


<tr>
<td class="org-left">-sm4-ctr</td>
<td class="org-left">-sm4-ecb</td>
<td class="org-left">-sm4-ofb</td>
</tr>
</tbody>
</table>

Par exemple, pour chiffrer un fichier en utilisant l'algorithme `blowfish` 
en mode CBC,  on utilise :

```bash
openssl enc -bf-cbc -in toto -out toto.chiffre

```

La clé et le vecteur d'initialisation sont générés grace au mot de
passe fourni.

Pour déchiffrer un message, on utilise 

```bash
openssl enc -bf-cbc -d -in toto.chiffre -out toto.dechiffre

```

Pour vérifier :

```bash
diff toto toto.dechiffre

```

**à faire**

Chiffrer, puis déchiffrer le fichier de votre choix, puis vérifier.


# RSA avec `openSSL`


## Génération d'une paire de clés

On peut générer une paire de clés RSA avec la commande `genrsa` de `openSSL` :

```bash
openssl genrsa -out <fichier> <taille>

```

où *fichier* est un nom de fichier de sauvegarde de la clé et
*taille* est la taille souhaitée (exprimée en bits) du modulus de
la clé.

Par exemple, pour générer une paire de clés de 2048 bits, stockée
dans le fichier `maCle.pem`, on saisit la commande suivante :

```bash
openssl genrsa -out maCle.pem 2048

```

Le fichier obtenu est un fichier au format PEM (Privacy Enhanced
Mail, format en base 64), dont voici un contenu possible :

```bash
cat maCle.pem

```

    -----BEGIN RSA PRIVATE KEY-----
    MIIEogIBAAKCAQEAo9RhpgjZRwHUeTeQrV4ydB2zfCcsEPiZB7iC97ayH3NrZ+Av
    7tAEoHLc8U71nPX3LUG0PIm6n5ZEiXvyyTaRNYnbF9cMA1B1d5k7jtpv0i1uGbs0
    t7O/4jniMpH9XN7FS9q6MkCjYSaOi7SNN4GsCOQ00BNMpke1kkA/PaJcOEgyttGe
    syxFZiVN37AoM2RmH8WxAilPEqPm6Wo+REGrB2JcKp6iJxIYTegLd4kgqLKmQHbB
    p0L7IsxeIYR+B3vZe3ga2op1l8xvtwbRqF16Wz2lWaPelzHVfSA8xltLoeHFbBA6
    FG1a5J638+lF+SbJcn++pGrcgNmiWdOHlxM8cwIDAQABAoIBAHZIf5wV5XHr81D1
    ekKBsSG+0/i+e4VhR3OYlw5sXJxyg8iCx3i/vaduZvDellyWBbkqKAxRUGmqX593
    2ynFeP/ToVjMDcNS0wzfmG2ibnusC0MOs5gyckbOje7/EpN7z6zdpfNkUdFxuhdJ
    62948xGUWkEkvgGL4p9OPZqtX7HN3uEeIZe2ZjiMJSwRSvSSCX+jAKrMl03x0pDC
    gSsfwg30J49Ka4nrfSKTp2kN4eoxC0du701MrfcUuwzkWafYzLC7s3zuVMsAA5Ii
    mwdRYw4xZzyRso81RWovx1lSc40JtlqW+06nP8BzlJI8F1hGYYK/WaTHWcj1gn06
    pdFVCckCgYEA0p8izTMCFdyAptxw7IFZe4Yw50g6qbql5kGDRV6Z8Yv76c3RRDPY
    8VEs8z4x5bRltR/8XrKwtuaZkAkEOHO+m912APy8u5jgJWg3wgVO75R66VMCinDr
    mu86podITX0thDwTpqFfU9zJnEkW/8WfclMlCcYF1l2QdAVYrE4mB98CgYEAxyBt
    B8yn6ThVvpLOqRvsL3g5UGls35lgDQ2cRFiSGpwPptGL0QaUyr2vgBknNsFn6kqC
    efVxSKgb4BvThHMICRG/v2nOsOpdZpNxc2zdZItBaixg2m8hbgaLvYIt79M1Az46
    7M7nGnt4hWx66SLtB0VaC1oaAF/JsJh7QFP8be0CgYBlTtwrfWR8ZXKQUCoU6GbF
    LpepN6nX6ApSRaLBpC5B7AZAwJnux02LTXNkkL7pcSsWwwrd9e9gGm00xdF84deG
    GzDearLRPUhbVdS0A1+jh3TTc/Ud6BC7lmRMo3eYpiRVnZwKI0a6DNIA1Xs1jbYT
    BIElQVWfdaO82rF7N5+9WwKBgHaqBS+Dr4CVOMvT0uIGKM71FzqrSN1LGmlM9hXo
    rlC7NbyU3XxYLq1PMtnko3DG+vUtJ+oj8Tl6LPp1uqwQnPZCpIEePbVCRRzOuuot
    MxYqwnXSMnmZhlN6BSaPAG8N72aaXCUo+6HVeLISPoDWdmhB1PI05I/ahWf+xlxE
    cIbVAoGAahGbvyRMdL+s3IslSkwqXIJ++QXXhjudE36z4c2xXDVLig2bjOin2TNo
    jTp/Z/Vwd4/UBOt8ukt0mHAJ2TM8GUnr997sDR8hVQ4W2W5J1DEMQJFhDk4RQ/eM
    pdGAscQqFKqGmeKPLCWMQhgXgsy4zriSCgYndsMmzwrpwSjpRsk=
    -----END RSA PRIVATE KEY-----


## Visualisation des clés RSA

La commande `rsa` permet d'afficher sur la sortie standard le
contenu d'un fichier au format PEM contenant une paire de clés RSA.

```bash
openssl rsa -in <fichier> -text -noout

```

L'option `-text` demande l'affiche décodé de la paire de
clés. L'option `-noout` supprime la sortie normalement produite par
la commande `rsa`.

Par exemple :

```bash
openssl rsa -in maCle.pem -text -noout

```

    RSA Private-Key: (2048 bit, 2 primes)
    modulus:
        00:a3:d4:61:a6:08:d9:47:01:d4:79:37:90:ad:5e:
        32:74:1d:b3:7c:27:2c:10:f8:99:07:b8:82:f7:b6:
        b2:1f:73:6b:67:e0:2f:ee:d0:04:a0:72:dc:f1:4e:
        f5:9c:f5:f7:2d:41:b4:3c:89:ba:9f:96:44:89:7b:
        f2:c9:36:91:35:89:db:17:d7:0c:03:50:75:77:99:
        3b:8e:da:6f:d2:2d:6e:19:bb:34:b7:b3:bf:e2:39:
        e2:32:91:fd:5c:de:c5:4b:da:ba:32:40:a3:61:26:
        8e:8b:b4:8d:37:81:ac:08:e4:34:d0:13:4c:a6:47:
        b5:92:40:3f:3d:a2:5c:38:48:32:b6:d1:9e:b3:2c:
        45:66:25:4d:df:b0:28:33:64:66:1f:c5:b1:02:29:
        4f:12:a3:e6:e9:6a:3e:44:41:ab:07:62:5c:2a:9e:
        a2:27:12:18:4d:e8:0b:77:89:20:a8:b2:a6:40:76:
        c1:a7:42:fb:22:cc:5e:21:84:7e:07:7b:d9:7b:78:
        1a:da:8a:75:97:cc:6f:b7:06:d1:a8:5d:7a:5b:3d:
        a5:59:a3:de:97:31:d5:7d:20:3c:c6:5b:4b:a1:e1:
        c5:6c:10:3a:14:6d:5a:e4:9e:b7:f3:e9:45:f9:26:
        c9:72:7f:be:a4:6a:dc:80:d9:a2:59:d3:87:97:13:
        3c:73
    publicExponent: 65537 (0x10001)
    privateExponent:
        76:48:7f:9c:15:e5:71:eb:f3:50:f5:7a:42:81:b1:
        21:be:d3:f8:be:7b:85:61:47:73:98:97:0e:6c:5c:
        9c:72:83:c8:82:c7:78:bf:bd:a7:6e:66:f0:de:96:
        5c:96:05:b9:2a:28:0c:51:50:69:aa:5f:9f:77:db:
        29:c5:78:ff:d3:a1:58:cc:0d:c3:52:d3:0c:df:98:
        6d:a2:6e:7b:ac:0b:43:0e:b3:98:32:72:46:ce:8d:
        ee:ff:12:93:7b:cf:ac:dd:a5:f3:64:51:d1:71:ba:
        17:49:eb:6f:78:f3:11:94:5a:41:24:be:01:8b:e2:
        9f:4e:3d:9a:ad:5f:b1:cd:de:e1:1e:21:97:b6:66:
        38:8c:25:2c:11:4a:f4:92:09:7f:a3:00:aa:cc:97:
        4d:f1:d2:90:c2:81:2b:1f:c2:0d:f4:27:8f:4a:6b:
        89:eb:7d:22:93:a7:69:0d:e1:ea:31:0b:47:6e:ef:
        4d:4c:ad:f7:14:bb:0c:e4:59:a7:d8:cc:b0:bb:b3:
        7c:ee:54:cb:00:03:92:22:9b:07:51:63:0e:31:67:
        3c:91:b2:8f:35:45:6a:2f:c7:59:52:73:8d:09:b6:
        5a:96:fb:4e:a7:3f:c0:73:94:92:3c:17:58:46:61:
        82:bf:59:a4:c7:59:c8:f5:82:7d:3a:a5:d1:55:09:
        c9
    prime1:
        00:d2:9f:22:cd:33:02:15:dc:80:a6:dc:70:ec:81:
        59:7b:86:30:e7:48:3a:a9:ba:a5:e6:41:83:45:5e:
        99:f1:8b:fb:e9:cd:d1:44:33:d8:f1:51:2c:f3:3e:
        31:e5:b4:65:b5:1f:fc:5e:b2:b0:b6:e6:99:90:09:
        04:38:73:be:9b:dd:76:00:fc:bc:bb:98:e0:25:68:
        37:c2:05:4e:ef:94:7a:e9:53:02:8a:70:eb:9a:ef:
        3a:a6:87:48:4d:7d:2d:84:3c:13:a6:a1:5f:53:dc:
        c9:9c:49:16:ff:c5:9f:72:53:25:09:c6:05:d6:5d:
        90:74:05:58:ac:4e:26:07:df
    prime2:
        00:c7:20:6d:07:cc:a7:e9:38:55:be:92:ce:a9:1b:
        ec:2f:78:39:50:69:6c:df:99:60:0d:0d:9c:44:58:
        92:1a:9c:0f:a6:d1:8b:d1:06:94:ca:bd:af:80:19:
        27:36:c1:67:ea:4a:82:79:f5:71:48:a8:1b:e0:1b:
        d3:84:73:08:09:11:bf:bf:69:ce:b0:ea:5d:66:93:
        71:73:6c:dd:64:8b:41:6a:2c:60:da:6f:21:6e:06:
        8b:bd:82:2d:ef:d3:35:03:3e:3a:ec:ce:e7:1a:7b:
        78:85:6c:7a:e9:22:ed:07:45:5a:0b:5a:1a:00:5f:
        c9:b0:98:7b:40:53:fc:6d:ed
    exponent1:
        65:4e:dc:2b:7d:64:7c:65:72:90:50:2a:14:e8:66:
        c5:2e:97:a9:37:a9:d7:e8:0a:52:45:a2:c1:a4:2e:
        41:ec:06:40:c0:99:ee:c7:4d:8b:4d:73:64:90:be:
        e9:71:2b:16:c3:0a:dd:f5:ef:60:1a:6d:34:c5:d1:
        7c:e1:d7:86:1b:30:de:6a:b2:d1:3d:48:5b:55:d4:
        b4:03:5f:a3:87:74:d3:73:f5:1d:e8:10:bb:96:64:
        4c:a3:77:98:a6:24:55:9d:9c:0a:23:46:ba:0c:d2:
        00:d5:7b:35:8d:b6:13:04:81:25:41:55:9f:75:a3:
        bc:da:b1:7b:37:9f:bd:5b
    exponent2:
        76:aa:05:2f:83:af:80:95:38:cb:d3:d2:e2:06:28:
        ce:f5:17:3a:ab:48:dd:4b:1a:69:4c:f6:15:e8:ae:
        50:bb:35:bc:94:dd:7c:58:2e:ad:4f:32:d9:e4:a3:
        70:c6:fa:f5:2d:27:ea:23:f1:39:7a:2c:fa:75:ba:
        ac:10:9c:f6:42:a4:81:1e:3d:b5:42:45:1c:ce:ba:
        ea:2d:33:16:2a:c2:75:d2:32:79:99:86:53:7a:05:
        26:8f:00:6f:0d:ef:66:9a:5c:25:28:fb:a1:d5:78:
        b2:12:3e:80:d6:76:68:41:d4:f2:34:e4:8f:da:85:
        67:fe:c6:5c:44:70:86:d5
    coefficient:
        6a:11:9b:bf:24:4c:74:bf:ac:dc:8b:25:4a:4c:2a:
        5c:82:7e:f9:05:d7:86:3b:9d:13:7e:b3:e1:cd:b1:
        5c:35:4b:8a:0d:9b:8c:e8:a7:d9:33:68:8d:3a:7f:
        67:f5:70:77:8f:d4:04:eb:7c:ba:4b:74:98:70:09:
        d9:33:3c:19:49:eb:f7:de:ec:0d:1f:21:55:0e:16:
        d9:6e:49:d4:31:0c:40:91:61:0e:4e:11:43:f7:8c:
        a5:d1:80:b1:c4:2a:14:aa:86:99:e2:8f:2c:25:8c:
        42:18:17:82:cc:b8:ce:b8:92:0a:06:27:76:c3:26:
        cf:0a:e9:c1:28:e9:46:c9

Les différents nombres constituant la clé sont affichés en
héxadécimal (hormis l'exposant public). On peut distinguer le
modulus, l'exposant public (qui par défaut est toujours 65537),
l'exposant privé, les nombres premiers facteurs du modulus, plus
trois autres nombres qui servent à optimiser l'algorithme de
déchiffrement.


## Chiffrement d'un fichier de clés RSA

Il n'est pas prudent de laisser une paire de clés en clair (surtout
la partie privée). Avec la commande `rsa`, il est possible de
chiffrer une paire de clés. Pour cela, trois options sont possibles
qui précisent l'algorithme de chiffrement symétrique à utiliser :
`-des`, `-des3` et `-idea`

    $ openssl rsa -in maCle.pem -des3 -out maCle.pem
    writing RSA key
    Enter PEM pass phrase:
    Verifying - Enter PEM pass phrase:

**à faire** 

Avec la commande `cat`, observez le contenu du fichier
`maCle.pem`. Utilisez à nouveau la commande `rsa` pour visualiser
le contenu de la clé.


## Exportation de la partie publique

La partie publique d'une paire de clés RSA est publique. À ce titre
elle peut être communiquée à n'importe qui.

Le fichier `maCle.pem` contient la partie privée de la clé, et ne peut 
donc pas être communiqué tel quel (même s'il est chiffré). Avec l'option
`-pubout` on peut exporter la partie publique d'une clé.

```bash
openssl rsa -in maCle.pem -pubout -out maClePublique.pem

```

**à faire**

-   Notez le contenu du fichier `maClePublique.pem`. Remarquez les
    marqueurs de début et de fin.
-   Avec la commande `rsa`, visualisez la clé publique contenu dans
    `maClePublique.pem`. Attention : vous devez préciser l'option
    `-pubin` puisque seule la partie publique figure dans le fichier
    `maClePublique.pem`.


## Chiffrement/Déchiffrement de données avec RSA

On peut chiffrer des données avec une clé RSA, bien que cela ne
soit **pas recommandé**. Pour cela, on utilise la commande `rsautl`.

```bash
openssl rsautl -encrypt -in <fichier_entree> -inkey <cle> -out <fichier_sortie>

```

où :

-   `fichier_entree` est le fichier des données à
    chiffrer. Attention, ce fichier ne doit pas avoir une taille
    excessive (ne doit pas dépasser 116 octets pour une clé de 1024
    bits) ;
-   `cle` est le fichier contenant la clé RSA. Si ce fichier ne
    contient que la partie publique de la clé, alors il faut rajouter
    l'option `-pubin` ;
-   `fichier_sortie` est le fichier de données chffré.

Pour déchiffrer, on remplace l'option `-encrypt` par `-decrypt`. Le
fichier contenant la clé doit évidemment contenir la partie privée.


## Chiffrement/Déchiffrement à l'aide de chiffrements symétriques

**à faire**

Chiffrez le fichier de votre choix avec le système symétrique de
votre choix. Chiffrez la clé ou le mot de passe utilisé(e) avec la
clé publique de votre destinataire (demandez-lui sa clé publique si
vous ne l'avez pas). Envoyez lui le mot de passe chiffré ainsi que le 
fichier chiffré.

**à faire**

Déchiffrez le message reçu.


## Signature de fichiers

Il n'est possible de signer que de petits documents. Pour signer un
gros document on calcule d'abord une empreinte de ce document. La
commande `dgst` permet de le faire.

```bash
openssl dgst <hachage> -out <empreinte> <fichier_entree>

```

où `hachage` est une fonction de hachage. Avec openssl, plusieurs
fonctions de hachage sont proposées dont : 

-   MD5 (option `-md5`), qui calcule des empreintes de 128 bits,
-   SHA1 (option `-sha1`), qui calcule des empreintes de 160 bits,
-   RIPEMD160 (option `-ripemd160`), qui calcule des empreintes de
    160 bits.

Signer un document revient à signer son empreinte. Pour cela, on
utilise l'option `-sign` de la commande `rsautl` 

```bash
openssl rsautl -sign -in <empreinte>  -inkey <cle>  -out <signature>

```

et pour vérifier la signature :

```bash
openssl rsautl -verify   -in <signature> -pubin \
        -inkey <cle>  -out <empreinte>

```

il reste ensuite à vérifier que l'empreinte ainsi produite est la
même que celle que l'on peut calculer. L'option `-pubin` indique que
la clé utilisée pour la vérification est la partie publique de la
clé utilisée pour la signature.

**à faire**
Signez le fichier de votre choix, puis vérifiez la signature.

**à faire** 

Récupérez l'archive [signatures.zip](./datas/signatures.zip) qui contient deux
fichiers accompagnés d'une signature.

fichier : cigale.txt, signature : signature1
et fichier : QuandLaMerMonte.txt, signature : signature2, 

ainsi que la partie publique de la clé RSA ayant produit la signature : uneClePublique.pem.

De ces deux fichiers, lequel a bien été signé ?


# Certificats


## Création d'une requête de certificats

Nous allons établir une requête pour obtnir un certificat.

En plus de votre clé publique, le certificat doit contenir des
informations sur l'identité de son propriétaire :

-   Pays (C),
-   État ou province (ST),
-   Ville ou localité (L),
-   Organisation (O),
-   Unité (OU),
-   Nom (CN),
-   Email

Un fichier de configuration ([req.cnf](./datas/req.cnf)) permet de fournir des
informations par défaut.

On établir la requête à l'aide de la commande `req` 

```bash
openssl req -config req.cnf -new -key maCle.pem -out maRequete.pem

```

Le fichier produit est aussi au format PEM :

```org
Certificate Request:
    Data:
        Version: 1 (0x0)
        Subject: C = FR, ST = Nord (59), L = Villeneuve d'Ascq, O = Universite de Lille 1, OU = Licence & Master  info, CN = Raymond Calbuth, emailAddress = Raymond.Calbuth@ronchin.fr
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:d4:b8:e5:2b:c8:ae:f7:7b:46:56:e8:b6:67:3a:
                    3e:7c:ba:76:e5:f0:50:2e:3b:ad:df:87:a7:b7:94:
                    68:41:ac:b8:33:e3:e4:31:08:12:70:27:39:da:1e:
                    10:b5:e3:19:63:e7:e2:dc:49:d4:9d:2f:03:b7:2c:
                    37:8a:fa:75:4a:37:d3:7d:fc:ac:5a:52:48:43:d8:
                    db:51:51:4b:dd:50:84:91:d1:10:df:e3:bb:2a:73:
                    b1:94:67:7c:11:df:37:56:77:b9:b9:77:2f:45:1b:
                    1b:43:57:ef:19:34:d2:3c:9f:cd:82:69:6d:90:c1:
                    63:ee:77:1e:84:3c:3e:08:23:9d:51:75:a6:9d:6b:
                    a8:ac:53:95:b3:9c:ff:e9:25:45:d2:0f:bf:35:9f:
                    52:43:84:7f:b6:ce:66:0a:89:65:d3:e4:ef:28:f3:
                    ca:7f:51:fd:b4:42:be:c3:4c:48:3c:4f:6a:8b:f5:
                    bf:0d:b9:77:68:fb:40:a1:a7:79:5d:2a:a8:50:fe:
                    fa:cd:0d:f5:9c:75:3f:46:4e:4a:49:7c:aa:37:31:
                    39:a3:95:7c:a3:eb:27:d4:50:f1:a3:3b:f8:5b:08:
                    6e:3a:42:ff:4d:fe:80:e9:78:9b:a8:bf:cf:a6:08:
                    99:e4:d6:97:03:25:9c:ce:9e:ba:f1:d5:a7:a8:e1:
                    56:d1
                Exponent: 65537 (0x10001)
        Attributes:
            a0:00
    Signature Algorithm: sha256WithRSAEncryption
         5a:42:4a:fd:b2:7d:9e:2a:80:88:50:36:86:73:df:ab:cc:92:
         9b:01:84:d6:18:0d:6f:22:0c:a1:51:ea:c2:07:e8:9e:8c:09:
         23:88:74:1e:82:11:68:bf:75:94:9a:e7:9c:9b:b9:ba:c8:1a:
         5d:b3:a4:5d:6b:ae:46:3a:71:4c:54:56:1d:b9:38:d9:28:f7:
         c2:f0:ea:35:bc:81:6e:c2:24:6c:74:2a:c4:00:f8:5c:45:b6:
         56:8a:ca:03:46:b0:a0:34:9d:4f:07:1a:a3:f1:aa:8d:2e:6a:
         d5:1c:f5:4e:7c:66:3d:fe:9a:14:d7:5d:a8:9a:75:1c:29:b6:
         70:62:be:a6:17:81:dc:5c:3a:3d:a0:d2:f9:45:f4:6d:aa:75:
         d3:8f:97:0d:f9:47:ee:a4:dc:5f:52:30:5c:74:2d:57:37:7c:
         4c:39:16:9e:94:b2:65:87:43:b1:62:6b:54:19:15:42:96:5f:
         c3:59:5f:ef:0d:cf:9c:b5:86:98:36:3f:52:1a:9f:3b:19:a1:
         c5:3c:da:24:03:b9:27:9e:05:4c:9b:e7:27:06:33:51:51:14:
         85:f9:aa:4c:dc:d4:32:c0:7b:02:fe:f8:66:f3:d2:58:cf:80:
         e9:f4:eb:4d:78:6a:f1:c3:50:59:b8:43:30:61:50:10:d3:ca:
         4b:d8:fe:5e

```


## Demande de signature de certificat

Une fois créé une requete de certificat, il reste à contacter une
autorité de certification qui vous délivrera un certificat signé,
après avoir procédé (normalement) à quelques vérifications vous
concernant.  


### L'autorité de certification Père Ubu

Vous jouerez dans ce TP le rôle de l'autorité de
certification. Pour cela il vous faut un certificat d'autorité de
certification, ainsi qu'une paire de clés. Pour ne pas multiplier
les autorités, vous utiliserez tous la très notable autorité *Père
Ubu* dont vous trouverez ici le [certificat](./datas/pereUbuCertif.pem) et la [paire de clés RSA](./datas/pereUbuCle.pem).

Un certificat produit par `openssl` est au format PEM. Pour le visualiser :

```bash
openssl x509 -in unCertif.pem -text -noout

```

**à faire** 

Après avoir récupéré le certificat de l'autorité, ainsi que sa
paire de clés RSA, cherchez quelle est la date d'expiration et la
taille de la clé.


### Création d'un certificat

Pour créer et signer un certificat à partir d'une requête
`maRequete.pem`, l'autorité invoque la commande `x509` 

```bash
openssl x509 -days 10\
        -CAserial PereUbu.srl \
        -CA PereUbuCertif.pem -CAkey pereUbuCle.pem \
        -extfile usr.ext -extensions x509_ext \
        -in maRequete.pem -req -out monCertif.pem

```

Dans laquelle 

-   l'option `-days` détermine la durée de validité du certificat
    (ici 10 jours) ;
-   `PereUbu.srl` est un fichier contenant le numéro de série du
    prochain certificat à signer (ce fichier est un fichier texte
    contenant une ligne donnant un nombre codé avec un nombre pair de
    chiffres hexadécimaux) ;
-   et `usr.ext` est un fichier contenant une
    description des extensions au certificat décrivant entre autres
    les usages auxquels sont destinés les certificats, l'option
    `-extension` précisant le nom de la section dans laquelle se
    trouvent les différentes extensions ([voici un exemple](./datas/usr.ext) d'un tel
    fichier).

**à faire**

Créez un certificat pour votre clé publique. (Lors de la signature
du certificat, la commande x509 invite l'autorité certifiante à
donner son mot de passe. Le mot de passe de Père Ubu ne devrait pas
vous être inconnu, puisqu'il s'agit DU mot du Père Ubu (celui
prononcé au tout début d'Ubu Roi). Néanmoins, le voici codé en base
64 `bWVyZHJlCg==` S'il le faut, utilisez la commande 
`openssl base64 -d`  pour le décoder.)

Puis contrôlez le contenu du certificat obtenu avec les options
appropriées de la commande `x509`.


### Vérification de certificats

On peut vérifier la validité d'un certificat avec la commande
`verify`. Pour vérifier la validité d'un certificat, il est nécessaire
de disposer du certificat de l'autorité qui l'a émis. 

```bash
openssl verify -CAfile pereUbuCertif.pem monCertif.pem

```

