DIU Enseigner l'informatique au lycée
=====================================

Univ. Lille

Récupérer et organiser des données réelles 
======================================

Dans ce TP, vous allez travailler sur des données réelles
[movies.csv](./movies.csv). Ces données en csv sont issues de
[data.world](https://data.world). 


## Partie 1: Réfléchir sur les données

Depuis DB Browser, créer une nouvelle base de données que vous pouvez appeler `movies`.
Créer une table `movie_metadata` contenant les données du csv [movies.csv](./movies.csv) (onglet Fichier, Importer, Table depuis un fichier csv). Lors de l'import, pensez à cocher la case "Nom des colonnes en première ligne" et à choisir comme séparateur la virgule.

Vous pouvez à présent visualiser les colonnes et les données contenues dans cette table. 

* À partir des données observées, imaginez un découpage des données permettant une décomposition plus claire.

## Partie 2: Création des tables et des contraintes

Nous allons utiliser le modèle suivant <img src="./movies.png" width="300"/>. 

* Il faut maintenant créer les tables manquantes et les remplir.

```sql
create table realisateur as
  select DISTINCT realisateur as nom_realisateur,
    realisateur_facebook_likes as likes
    from movie_metadata;
```

Vous pouvez vérifier qu'il y a pas de doublon (deux fois le même nom) dans la nouvelle table créée avec la requête suivante
(complètement hors programme NSI):

```sql
select nom_realisateur, count(*)
  from realisateur
  group by nom_realisateur
  having count(*)>1; 
```

Elle ne doit retourner aucun enregistrement.

Si c'est le cas, vous pouvez définir `nom_realisateur` comme clé primaire de la table `realisateur` (onglet "Structure de la Bases de données", "modifier la table", cochez CP comme Clé Primaire).

* Quel est l'ensemble des résultats que retourne la requête suivante?

```sql
select DISTINCT acteur1 as nom_acteur,
  acteur1_facebook_likes as likes
  from movie_metadata UNION
    select DISTINCT acteur2 as nom_acteur,
      acteur2_facebook_likes as likes
      from movie_metadata;
```
	  
Inspirez vous de cette requête, et de la requête qui nous a permis
de créer la table `realisateur`, pour créer et remplir la table `acteur`.
N'oubliez pas qu'il y a 3 acteurs cités par film.
Une fois la table créée et remplie, ajoutez la clé primaire.


Solution


```sql
create table acteur as
  select DISTINCT acteur1 as nom_acteur,
    acteur1_facebook_likes as likes
    from movie_metadata UNION
      select DISTINCT acteur2 as nom_acteur,
      acteur2_facebook_likes as likes
      from movie_metadata UNION
	select DISTINCT acteur3 as nom_acteur,
	acteur3_facebook_likes as likes
	from movie_metadata ;
```


* Il reste la table `film`. Deux solutions sont possibles: renommer la table `movie_data` en `film`  puis supprimer les colonnes devenues inutiles, ou créer une nouvelle table en ne sélectionnant que les colonnes pertinentes (sur le modèle des tables précédentes). Ajouter la clé primaire et les clés étrangères.
Pour créer les clés étrangères, il faut double-cliquer sur la ligne
qui correspond à l'attribut que l'on veut au niveau de la colonne "Clé étrangère". Cela donne accès à des menus déroulants qui permettent de choisir la table et la colonne à laquelle la clé fait référence puis
presser la touche `entrer` pour valider la clé.


## Partie 3: Quelques requêtes

* Tous les acteurs du film Ben-Hur
(attention, le titre contient un espace à la fin de la chaîne de caractères)


Solution

```sql
SELECT acteur1, acteur2, acteur3 from film where titre='Ben-Hur ';
```



* Tous les films de Brad Pitt


Solution

```sql
SELECT titre from film where acteur1='Brad Pitt' OR acteur2='Brad Pitt' OR acteur3='Brad Pitt';
```


* Les films réalisés par Tim Burton dans lesquels Johnny Depp est l'acteur 1.


Solution

```sql
SELECT titre from film where realisateur='Tim Burton' AND acteur1='Johnny Depp';
```

 
* Les films qui n'ont pas d'acteur3. Existe-t-il des films sans aucun acteur?


Solution

```sql
SELECT titre from film where acteur3 IS NULL;

SELECT titre from film where acteur1 IS NULL AND acteur2 IS NULL AND acteur3 IS NULL;

```


 

* Les acteurs qui ont plus de 10.000 likes, par ordre décroissant.

Solution:

```sql
SELECT nom_acteur from acteur where likes>10000 ORDER BY likes DESC;
```


* Les films dont le réalisateur a plus de 5.000 likes, par ordre décroissant.


Solution:

```sql
SELECT titre from film JOIN realisateur ON realisateur=nom_realisateur where likes>5000 ORDER BY likes DESC;
```


* Les acteurs dont le nombre de like est pair.


Solution:

```sql
SELECT nom_acteur from acteur where likes%2=0;
```



* Le nombre de likes pour ces acteurs dont le nombre de likes est
  pair.


Solution:

```sql
SELECT COUNT(*) from acteur where likes%2=0;
```



* Les acteurs qui sont acteur1 d'un film et acteur2 d'un autre film.


Solution:

```sql
SELECT DISTINCT f1.acteur1, f2.acteur2 FROM film AS f1 JOIN film AS f2 ON f1.acteur1=f2.acteur2;
```



Les deux requêtes suivantes nécessitent un `GROUP BY`, elles ne sont donc pas au programme pour les élèves.

* Coût moyen des films par réalisateur, du moins cher au plus cher.
 

Solution:

```sql
SELECT realisateur, AVG(budget) AS budget_moyen FROM film GROUP BY realisateur ORDER BY AVG(budget) ASC;
```



* Le nombre de films par année, du plus grand nombre vers le plus petit 


Solution:

```sql
SELECT annee,COUNT(*) AS nb_film FROM film GROUP BY annee ORDER BY COUNT(*) DESC;
```



## Partie 4: Depuis un programme ? 

Petit challenge: les requêtes suivantes peuvent-elles être faites uniquement en SQL ou un programme est-il nécessaire?

* Le pays dans lequel le plus de films ont été réalisés.


Solution: En fait on peut faire les deux,

```sql
SELECT pays FROM film GROUP BY pays ORDER BY COUNT(*) DESC LIMIT 1;
```

Ou en pgm avec un `fectchone`.


* Le pourcentage de films par pays. 


Pas le choix... Un pgm est nécessaire

La requête précédente sans le limit fournit la valeur, avant on récupère le total, puis on calcule le %


* Y a t-il plus de likes pairs ou de likes impairs chez les réalisateurs?


Pas le choix... Un pgm puis un test est nécessaire


* Les réalisateurs qui sont aussi acteurs (de leur film ou d'un autre)


Solution, une requête est possible

```sql
SELECT DISTINCT f1.realisateur FROM film AS f1 JOIN film AS f2 ON f1.realisateur=f2.acteur1 OR  f1.realisateur=f2.acteur2 OR  f1.realisateur=f2.acteur3;
```



