#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'DiU EIL univ. Lille'
__date_creation__ = 'Nov 2020'
__doc__ = """
:mod:`binary_tree` module
:author: {:s} 
:creation date: {:s}
:last revision:

Define a class for automata
""".format(__author__, __date_creation__)

import time
import graphviz

WHITE = '#FFFFFF'
BLACK = '#000000'

class StateDict:
    def __init__(self, automata, state ):
        """
        crée un dictionnaire pour un état
        :param automata: (Automata) un automate
        :param state: (int) l'état
        """
        self.__automata = automata
        self.__state = state
    #
    def __setitem__(self, key, item):
        """
        assigne un item à une clé
        :param key! (any) un symbole
        :param item: (any) l'état
        """
        self.__automata[ (self.__state, key) ] = item
    #
    def __getitem__(self, key):
        """
        renvoie l'état associé au symbole
        :param key: (any) le symbole
        """
        return self.__automata[ (self.__state, key) ]
    #
    def __delitem__(self, key):
        """
        supprime une association
        :param key! (any) le symbole
        """
        del self.__automata[ (self.__state, key) ]

class Automata:
    def __init__(self, initial_state, final_states,
                 transitions = None ):
        """
        :param states: (set) les états
        :param alphabet: (set) les symboles
        :param transitions: (dict) la fonction de transition
        :initial_state: (any) l'état initial 
        :final_states: (set) l'ensemble des états finaux

        :CU: set(transitions.keys()) == states and
             all(set(transitions[q].keys()) == alphabet)
        """
        self.__states = set()
        self.__state = self.__initial_state = initial_state
        self.__states.add( self.__state )
        self.__final_states = final_states
        self.__states = self.__states.union(self.__final_states)
        self.__transitions = {}
        self.__alphabet = set()
        if transitions != None:
            for q, t in transitions.items():
                self[ q ] = t
    #
    def state(self):
        """
        :return: (any) l'état de l'automate
        """
        return self.__state
    #
    def set_final(self, state):
        self.__states.add(state)
        self.__final_states.add(state)
    #
    def is_final(self):
        """
        :return: (bool) True si l'automate est dans un état final, 
                 False, sinon.
        """
        return self.__state in self.__final_states
    #
    def reset(self):
        """
        réinitialise l'état
        """
        self.__state = self.__initial_state
    #
    def next(self, l):
        """
        calcule l'état suivant

        :param l: (any) un symbole de l'alphabet
        """
        if (self.__state, l) in self.__transitions:
            self.__state = self[self.__state][l]
            return self
        # recherche d'une transition !=l
        for symbol in self.__alphabet:
            if symbol != l and (self.__state, '!=' + symbol) in self.__transitions\
               and (self.__state != self[self.__state]['!='+symbol]):
                self.__state = self[self.__state]['!=' + symbol]
                self.next(l)
                return self
        return self
    #
    def accept(self, word):
        """
        :param word: (iterable) un iterable de symboles
        :return: (bool) True si l'automate accepte le mot, False sinon
        """
        tmp = self.__state
        self.__state = self.__initial_state
        for l in word:
            self.next(l)
        res = self.is_final()
        self.__state = tmp
        return res
    #
    def __state_to_dot(self, state):
        color, shape = "black", "circle"
        if state in self.__final_states:
            shape = "doublecircle"
        if state == self.__state:
            color = "red"
        res = "node [shape={}, color={}, fontcolor=black]; {};".format(shape, color, state)
        return res
    #
    def __setitem__(self, key, item):
        """
        ajoute des transitions pour un état

        :param key: (any) un état
        :param item: (dict) les transitions
        :Exemple:

        >>> auto = Automata('a', 'b')
        >>> auto['a'] = { 0 : 'b'} 
        >>> auto['a'][0] == 'b'
        True
        """
        if isinstance(key, tuple) and len(key) == 2:
            s, l = key
            self.__states.add( s )
            self.__states.add( item )
            if l.startswith('!='):
                self.__alphabet.add( l[2:] )
            else:
                self.__alphabet.add(l)
            self.__transitions[(s, l)] = item
        else:
            for l, s in item.items():
                self.__setitem__( (key, l) , s )
    #
    def __getitem__(self, key):
        """
        renvoie une transition

        :param key: (any) un état
        :return: (dict) les transitions pour cet état
        """
        if isinstance(key, tuple) and len(key) == 2:
            s, l  = key
            res = self.__transitions[ (s, l) ]
        else:
            res = StateDict(self, key)
        return res
    #
    def __delitem__(self, key):
        """
        supprime une association
        :param key: (tuple) un couple (etat, symbole) à supprimer
        """
        if isinstance(key, tuple) and len(key) == 2:
            s, l = key
            del self.__transitions[ key ]
    #
    def to_dot(self):
        """
        :return: (str) a representation of automata
        """
        nodes = "\n".join( self.__state_to_dot(state) for state in self.__states)
        transitions = "\n".join( "{} -> {}[label=\"{}\"];".format(a, self.__transitions[ (a, l) ], l)
                                 for a, l in self.__transitions )
        return """
digraph {{
  rankdir=LR;
  node [shape=point, color=white, fontcolor=white]; start;
  {}
  start -> {};
  {}
}}""".format(nodes, self.__initial_state, transitions)
    #
    def show(self, filename='tree', background_color=WHITE):
        '''
        visualise l'automate et produit deux fichiers : filename et filename.png
        le premier contenant la description de l'tree au format dot, 
        le second contenant l'image au format PNG.
        '''
        descr = self.to_dot()
        graphviz.Source(descr, format='png').view(filename=filename)
    #
    def save(self, filename='automate', background_color=WHITE):
        '''
        produit deux fichiers : filename et filename.png
        le premier contenant la description de l'automate au format dot, 
        le second contenant l'image au format PNG.
        '''
        graphviz.Source(self.to_dot(),
                        format='png').render(filename=filename)
    #
    def _repr_png_(self):
        """
        """
        self.save('automate')
        return open('automate.png', 'rb').read()


if __name__ == "__main__":
    import doctest
    doctest.testmod()
