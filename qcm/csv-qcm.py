import csv

# fields of a question 
FIELDNAMES = ['theme', 'question', 'reponse', 'leurre1', 'leurre2', 'leurre3', 'leurresn', 'commentaire']

# topics string
TOPICSTRINGS = [
    "no topic 0",
    "Représentation des données : types et valeurs de base",
    "Représentation des données : types construits",
    "Traitement de données en tables",
    "Interactions entre l’homme et la machine sur le Web",
    "Architectures matérielles et systèmes d’exploitation",
    "Langages et programmation",
    "Algorithmique"]

def questions_of_file(filename) :
    '''a dictionnay of all the questions if CSV files'''
    dict = {}
    idx = 1
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=FIELDNAMES, delimiter=';')
        reader.__next__() # skip first (~empty) line
        reader.__next__() # skip sezcond (~empty) line
        header = reader.__next__() # skip fields #TODO : check fields
        assert (header['theme'] == 'thème' and header['question'] == 'question')
        for question in reader :
            dict[idx] = question
            idx += 1
    return dict

def print_raw_question(question) :
    for field, value in question.items():
        print (field, value)
    return

def md_topic(question) :
    '#' + str(question['theme'])
    return 

def md_header(subtitle=None) :
    '''markdown header string''' 
    import locale
    from datetime import datetime
    locale.resetlocale()                  # in french
    datestr = datetime.today().strftime('%A %d %B %Y - %X')
    if subtitle == None :
        subtitle_line = ''
    else :
        subtitle_line = "subtitle: " + subtitle + "\n"
    return ('---\n'
                + 'title: Récolte de questions QCM\n'
                + subtitle_line
                + 'author: participants au DIU EIL Lille\n'
                + 'date: ' + datestr + '\n'
                + '---\n')

def topic_idx_of_question(question) :
    '''theme field begins with a single digit number'''
    return int(question['theme'][0])

def is_of_topic(topic, question) :
    return topic_idx_of_question(question) == topic

def apply_on_topic(questions, topic, f) :
    '''apply function f on each question of the given topic'''
    for key, question in questions.items():
        if is_of_topic(question, topic) :
            f(question)
            
def markdown_of_question(question) :
    return question['question'] + "\n"

def markdown_of_answer(question) :
    return "* [x] " + question['reponse'] + '\n'

def markdown_of_lure(lure) :
    if (not lure) or lure.isspace() :      # not even a non-space
        return ''
    return "* [ ] " + lure + '\n'

def markdown_of_lures(question) :
    return (markdown_of_lure(question['leurre1'])
                + markdown_of_lure(question['leurre2'])
                + markdown_of_lure(question['leurre3'])
                + markdown_of_lure(question['leurresn']))
                                               
def markdown_of_comment(question) :
    comment = question['commentaire']
    if (not comment) or comment.isspace() : # not even a non-space
        return ''
    return '_Commentaire : ' + comment + '_\n\n'
        
def markdown_of_one_question(question, topic_idx, question_idx) :
    title = "## question " + str(topic_idx) + "." + str(question_idx) + " ##\n"
    nl = '\n'
    return (title + nl
                + markdown_of_question(question) + nl
                + markdown_of_answer(question)
                + markdown_of_lures(question) + nl
                + markdown_of_comment(question)
                + nl)

def markdown_of_a_topic_starting_at(questions, topic, start) :
    '''list of markdown for last questions of a given topic.'''
    markdown = []
    index = 1
    for question in questions.values() :
        if is_of_topic(topic, question) : 
            if (index >= start) :
                markdown.append(markdown_of_one_question(question, topic, index))
            index += 1
    return markdown
        
def markdown_of_a_topic(questions, topic) :
    '''list of markdown for questions of a given topic.'''
    return markdown_of_a_topic_starting_at(questions, topic, 1)

def markdown_header_of_a_topic(topic) :
    return "# Thème " + str(topic) + " - " + TOPICSTRINGS[topic] + "\n"

def print_questions(questions, topic_list) :
    subtitle = "topics " + str(topic_list)
    print(md_header(subtitle))
    for topic in topic_list :
        print(markdown_header_of_a_topic(topic))
        for md in markdown_of_a_topic(questions, topic) :
            print(md)

def print_last_questions(questions, topic_list, starting_list) :
    assert (len(topic_list) == len(starting_list))
    subtitle = ("thèmes " + str(topic_list)
                    + " - débutants questions " + str(starting_list))
    print(md_header(subtitle))
    for i, topic in enumerate(topic_list) :
        print(markdown_header_of_a_topic(topic))
        for md in markdown_of_a_topic_starting_at(questions, topic, starting_list[i]) :
            print(md)
            
d = questions_of_file('qcm_nsi_1re.csv')
# print_questions(d, range(1, 8))
# print_last_questions(d, range(1, 8), [24, 13, 3, 1, 12, 3, 6])
# print_last_questions(d, range(1, 8), [26, 15, 5, 1, 12, 3, 6])
# print_last_questions(d, range(1, 8), [29, 20, 5, 1, 12, 4, 10])
