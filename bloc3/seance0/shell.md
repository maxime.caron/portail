Initiation à UNIX, à l'interpréteur de commandes
================================================

L'objet de ce 1er TP est double

* découvrir l'environnement qui sera utilisé lors de la formation
* découvrir l'interpréteur de commandes UNIX et les "_commandes de base
  en ligne de commande_", capacité attendue du
  [programme de 1re NSI](http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138157). 

Nous travaillerons sous GNU/Linux, en accord avec le fait que "_Les élèves
utilisent un système d’exploitation libre_".

Nous aborderons à peine la gestion des "_droits et permissions d’accès
aux fichiers._" sur lesquels nous reviendrons. 

### organisation de la séance, du document

1. Dans un premier temps, nous allons parcourir le document
   d'introduction à l'interpréteur de commandes UNIX \
   → [Brève introduction à UNIX, à l'interpréteur de commandes](#1-très-et-trop-brève-introduction)
1. Nous exécuterons ensuite pas à pas une série de commandes et en
   observerons le résultat \
   → [Découverte de l'interpréteur de commande](#2-d%C3%A9couverte-de-linterpr%C3%A9teur-de-commandes)
1. Nous découvrirons la forge GitLab qui sera utilisée tout au long de
   la formation \
   → [Introduction à GitLab](#3-découverte-de-lenvironnement-gitlab)
1. Nous utiliserons l'interpréteur de commandes et ferons quelques
   exercices \
   → [Utilisation de l'interpréteur de commandes](#4-utiliser-linterpréteur-de-commandes)

    - [Chemins dans le système de fichiers](#-chemins-dans-le-syst%C3%A8me-de-fichiers)
    - [Manipulation du système de fichiers](#-manipulation-du-syst%C3%A8me-de-fichiers)
    - [Jokers pour les noms de fichier](#-jokers-pour-les-noms-de-fichier)
    - [Droits]()
    - [Entrées-sorties et redirections](#-droits)
    - [Commandes et processus](#-commandes-et-processus)
    - [Filtres](#-filtres)
    - [Variables d'environnement et substitutions](#-variables-denvironnement-et-substitutions)

1. Pour aller plus loin, nous découvrirons éventuellement les scripts
   shell 
   + [Les scripts](#5-les-scripts) 

1 — Très et trop brève introduction
===================================

On trouvera une brève introduction à l'interpréteur de commandes UNIX
dans le fichier [shell-bref.md](shell-bref.md). 

On pourra y découvrir UNIX, le shell - interpréteur de commandes -, et
quelques-unes des commandes de base. \
On pourra s'y référer par la suite. 


2 — Découverte de l'interpréteur de commandes
=============================================

→ Un terminal pour nos premiers pas
---------------------------------

Identifiez-vous auprès du système avec les login et mot de passe qui
vous ont été fourni. \
Vous accédez à un environnement graphique.
Les manipulations proposées ici seront réalisées via l'interpréteur de
commandes, dans un terminal. 

Un des terminaux installés sur les machines du FIL s'appelle `tilix`,
vous le trouverez dans la barre d'icônes ou dans le menu des
applications. 

Ouvrez un terminal.

→ Exécutions pas à pas
--------------------

Les commandes qui suivent sont donc *à saisir dans le terminal*. \
Validez ensuite en appuyant sur Entrée et observez l'action effectuée.

Lorsque vous entrez des commandes dans un terminal, celles-ci s'exécutent dans
le répertoire courant, par défaut la racine de votre répertoire personnel.

1. Le contenu d'un répertoire peut être obtenu au moyen de la commande
   `ls`. \
   Saisissez cette commande, le terminal affiche les fichiers présents dans le 
   répertoire courant.
1. Nous allons maintenant créer un nouveau répertoire : utilisez la
   commande `mkdir` (make directory) suivie du nom du répertoire. \
   Ici `mkdir bloc3`. Observez le résultat par une nouvelle exécution de `ls`. 
1. Déplaçons nous dans le répertoire `bloc3` nouvellement créé : `cd
   bloc3` (change directory). Constatez que l'invite de l'interpréteur
   a été modifiée, indiquant que nous sommes maintenant dans le
   répertoire `bloc3`. 
1. Utilisez la commande `pwd` pour afficher le répertoire courant.
1. Pour les besoins du TP, 
   * créons un nouveau répertoire : `mkdir tim`
   * plaçons nous dans ce répertoire : `cd tim`
   * affichons le répertoire courant : `pwd`
1. Nous allons créer un fichier dans le répertoire `tim` :
   * la commande `touch oleon` crée un fichier (vide) appelé `oleon`,
   * vérifions à l'aide de la commande `ls` que le fichier a bien été créé.
1. Remontons dans l'arborescence : 
   * `cd ..` 
   * vérifions que le répertoire courant a bien été modifié : `pwd`
   * créons un deuxième répertoire : `mkdir raymond`
1. Pour *copier un fichier* :
   * on utilise la commande `cp` : `cp tim/oleon raymond/`
   * cette commande a pour effet de copier le fichier `oleon` dans le 
	 répertoire `raymond/`
   * constatez son effet : `ls raymond/` qui affiche le contenu du répertoire
	 `raymond`
1. Nous allons maintenant tenter de supprimer le répertoire `tim` : 
   * `rmdir tim` (remove directory) vous constatez que la commande
	 produit une erreur puisque le répertoire n'est pas vide.
   * il est possible de supprimer un répertoire non vide, mais pour 
	 l'instant allons effacer le fichier `oleon` qui n'est de toute façon 
	 pas d'une grande utilité : `cd tim` puis `rm oleon` (remove).
1. Tentons à nouveau la suppression du répertoire : 
    * `cd ..` 
    * `rmdir tim`
	
	Constatez que la commande ne signale plus d'erreur et que le 
	répertoire a été supprimé.
1. Nous pouvons également déplacer des fichiers : 
    * `mv raymond/oleon .`
   
   Cette commande déplace (move) le fichier `oleon` de `raymond` dans 
   le répertoire courant `./` (ici `bloc3`).
1. La même commande `mv` permet aussi de renommer un fichier. Essayez
   `mv oleon arthur`. Observez. 
1. Le répertoire `raymond` ne contient plus rien : 
   * `ls raymond/` 
   * nous pouvons le supprimer : `rmdir raymond`
1. Saisissons les commandes suivantes :
   * `cp arthur Arthur`
   * `ls`
   
   Observez que le répertoire contient maintenant deux fichiers *différents*
   dont les noms sont `arthur` et `Arthur` : Le système de fichier est donc
   *sensible à la casse*.
1. Saisissons :
   * `cp arthur .arthur`
   * `ls`
   
   Observez que le contenu du répertoire ne semble pas avoir été
   modifié. Pourtant le fichier `.arthur` a bien été créé :
   * `ls .arthur`
   
   Il s'agit d'un *fichier caché*. 
   La convention adoptée et que tous les fichiers dont le nom commence 
   par un point `.` sont traités comme des fichiers cachés.
1. Pour visualiser *tous* les fichiers d'un répertoire (y compris les cachés) :
   * `ls -a`
   * ou `ls -al` 
   
   Observez la différence entre les deux commandes.  
   Testez `ls` avec l'option `-l` seule. En déduire l'effet de l'option `-l`. 
1. Observons que notre répertoire contient une entrée nommée `.` qui représente 
   le répertoire courant et une entrée nommée `..` qui correspond au répertoire
   supérieur. On retrouve ces deux entrées dans tous les répertoires.
1. Nettoyons maintenant notre répertoire :
   * `rm arthur Arthur .arthur` (la commande `rm` accepte plusieurs arguments)
   * `cd ..`
   * `rmdir bloc3`

3 — Découverte de l'environnement GitLab
========================================

Voir le document _ad hoc_ [gitlab.md](gitlab.md).

4 — Utiliser l'interpréteur de commandes
========================================

→ Chemins dans le système de fichiers
-------------------------------------

Tout fichier est identifiable par son nom et son emplacement
dans la hiérarchie.  
On appelle cette information le chemin (_path_ en anglais) du
fichier.

Un emplacement dans la hiérarchie est une liste de répertoires
qu’il faut traverser avant de parvenir dans le répertoire contenant le
fichier.  
Chaque répertoire d’un chemin est séparé du répertoire
suivant par le caractère « `/` ».  
La hiérarchie est organisée à partir d'une racine nommée `/`. 

Il existe deux manières de donner un chemin :

- en spécifiant la liste des répertoires à traverser *depuis la racine
  de la hiérarchie*.  
  On dit alors que c’est un **chemin absolu**.  
  Il débute par `/`. 
- en spécifiant une liste des répertoires à traverser à partir d’un
  répertoire particulier de la hiérarchie.  
  On dit dans ce cas que c’est un **chemin relatif** à ce répertoire
  de départ. 

### exercices ###

1. Affichez le répertoire courant (celui où vous vous trouvez).
2. Affichez (sans vous déplacer) le contenu du répertoire racine.
3. Déplacez vous dans le répertoire `etc` situé sous la racine en
   utilisant un nom absolu. 
4. Déplacez vous dans votre répertoire de connexion en utilisant un
   nom relatif.

→ Manipulation du système de fichiers
-------------------------------------

1. Créez un répertoire `systeme` et placez vous dedans.
2. Visitez le projet GitLab
   https://gitlab-fil.univ-lille.fr/diu-eil-lil/shell.  
   Faites un fork du projet (dans l'interface GitLab).  
   Ensuite, clonez votre projet dans le répertoire courant.
3. Placez vous vous dans le répertoire `fichiers`.
4. Utiliser la commande `tree` pour visualiser l'arborescence présente
   dans ce répertoire. Le contenu des fichiers correspond à leur nom.
4. Placez vous dans le répertoire `a_faire`. Donnez le chemin absolu
   du fichier `gestion` et les chemins relatifs des fichiers `algo` et
   `anglais`. Vérifiez qu’ils sont corrects en utilisant la commande `cat`
   pour visualiser leurs contenus en utilisant ces chemins.

→ Jokers pour les noms de fichier
---------------------------------

Un ensemble de méta-caractères, _jokers_, permet de spécifier des
modèles de noms de fichiers. Le shell remplace ces modèles par les
noms des fichiers correspondants : 

+ `*` remplace une suite quelconque de caractères (éventuellement vide)
+ `?` remplace un et un seul caractère quelconque
+ `[liste]` remplace n'importe quel caractère de *liste*
+ `[^liste]` remplace n'importe quel caractère **pas** dans la
  liste
+ `[car1-car2]` remplace les caractères contenus dans l'intervalle
    car1 - car2

### exercices ###

1. Placez vous dans le répertoire `substitutions`
2. Afficher la liste des fichiers

    + dont le nom commence par un `f`
    + dont le nom possède quatre lettres et qui ont un `t` en première
      et troisième lettre 
    + dont le nom a exactement quatre lettres
    + dont la première lettre n'est pas une minuscule

→ (Pour aller plus loin... — Liens)
-----------------------------------

Les informations sur les fichiers sont gérées par le système sous
forme de [nœud d'index ou
*inode*](https://fr.wikipedia.org/wiki/N%C5%93ud_d%27index). Chaque
inode est identifié par un numéro. Un répertoire est en fait un
fichier qui contient une liste de numéros d'inodes associés à des noms.

1. Réalisez la suite d’opérations demandées ci-après. Pour toutes ces
   commandes, votre répertoire de travail doit rester `a_faire`. Vous
   pouvez utiliser la commande `ls -i` (ou `tree --inodes`) pour
   vérifier l'effet de ces commandes en terme d'inodes.
   + recopiez le contenu du fichier `algo2` dans le fichier `algorithmique`
   + recopiez le fichier `algo2` dans le fichier `algo` du répertoire `fait`
   + renommez le fichier `anglais` en `english`
   + déplacez le fichier `english` dans le répertoire `fait`
   + faites en sorte que le fichier `math` s’appelle également `abandon`
	 dans le répertoire `fait` (i.e., on créer un lien physique)
   + éditez le fichier `abandon` et y mettre le mot `regret`.
   + visualisez le contenu du fichier `math`.
   + avec la commande `ls -l`, regardez le nombre de liens physiques du
     fichier `math`. A quoi correspondent-ils ?
   + créez un lien symbolique nommé ̀persevere` pointant sur le fichier
     `abandon`.
   + avec la commande `ls`, regardez le nombre de liens physiques du
     fichier ̀math`.
   + visualisez le contenu du fichier `persevere`.
   + supprimez le fichier `abandon`.
   + visualisez le contenu du fichier `persevere`.
   + faites en sorte que le fichier `math` s’appelle également
	 `sauvegarde` dans le répertoire `/tmp`. Expliquez le problème et
	 proposez une autre solution.
2. Déterminez le nombre de liens physiques pour le répertoire
   `a_faire` et à quoi ils correspondent.
	 
→ Droits
--------

À chaque fichier sont associés des droits qui déterminent ce que
chaque utilisateur a le droit de faire du fichier : le lire, le
modifier, l'exécuter, etc. 

Voyez la description de la commande `chmod` sur la page Wikipedia ad
hoc [chmod](https://fr.wikipedia.org/wiki/Chmod). 

Voyez éventuellement la page Wikipedia
[Permission UNIX](https://fr.wikipedia.org/wiki/Permissions_UNIX), et
en particulier la section
[Fonctionnement](https://fr.wikipedia.org/wiki/Permissions_UNIX#Fonctionnement).

1. Placez vous dans le répertoire `fichiers`.
2. Utilisez la commande `id` pour visualiser votre identifiant
   d'utilisateur (uid), votre groupe principal (gid) ainsi que les
   groupes auxquels vous appartenez.
3.  Créez un répertoire `prive` dans lequel vous créerez un fichier
	nommé `prive` contenant votre nom de login et un répertoire `partage`.
	+ En utilisant la **forme symbolique**, interdire l’accès au
	répertoire `prive` pour les membres du groupe et les autres.
	+ Dans le répertoire `partage` créez un fichier `lecture` dans lequel
	vous mettrez votre nom de login. Ce fichier devra être consultable
	mais non modifiable par les membres de votre groupe principal et
	non lisible/modifiable par les autres. Modifiez les droits en
	utilisant la **forme numérique**.
	+ Dans le répertoire `partage` créez un fichier `ecriture` dans
	lequel vous mettrez votre nom de login. Ce fichier devra être
	consultable et modifiable par les membres de votre groupe
	principal mais pas par les autres. Modifiez les droits en
	utilisant la **forme numérique**.
4. Demander à votre voisin de tester vos droits en :
   + Essayant de lire le contenu du fichier `prive`.
   + Essayant de lire puis de modifier le contenu du fichier `lecture`.
   + Ajoutant son nom de login à votre fichier `ecriture`
5. Éditez un fichier nommé `salut` avec le contenu suivant :
   ```
   echo Hello World
   ```
   Tentez de l'exécuter en tapant `./salut`. Modifiez les droits sous
   **forme symbolique** de manière à ce que vous puissiez l'exécuter
   puis vérifiez que vous pouvez l'exécuter.

→ Entrées-sorties et redirections
---------------------------------

Chaque exécution d'une commande ou d'un programme gère une table
identifiant les différents fichiers il utilise. Les index de cette table sont appelés descripteurs de fichiers. Par convention les trois premiers
descripteurs correspondent à :

* **l’entrée standard** - descripteur 0 : si le programme exécuté par
   le processus a besoin demande des informations à l’utilisateur,
   elle seront lues dans ce fichier (par défaut c’est le terminal,
   i.e. le clavier). 
* **la sortie standard** - descripteur 1 : si le programme affiche des
   des informations à l’utilisateur, elle seront écrites dans ce
   fichier (par défaut c’est le terminal).
* **la sortie d’erreur** - descripteur 2 : si le programme envoie un
   message d’erreur à l’utilisateur, il sera écrit dans ce fichier
   (par défaut c’est le terminal).
   
Le shell peut rediriger ces entrées-sorties vers n'importe quel
fichier au moyen de la syntaxe suivante :

notation  | effet
--------- | -----
`n<fichier` | redirige en **lecture** le descripteur *n* sur *fichier*
`n>fichier` | redirige en **écriture** le descripteur *n* sur *fichier*
`n>>fichier` | redirige en **écriture** le descripteur *n* sur *fichier* en **concaténant** à la fin de *fichier*
`n<&m` | copie le descripteur *m* sur le descripteur *n* en **lecture** (i.e. *n* et *m* correspondent au même fichier)
`n>&m` | copie le descripteur *m* sur le descripteur *n* en **écriture**
`n<<marque` | redirige en **lecture** le descripteur *n* jusqu'à ce que *marque* soit lu

En pratique on utilise le plus souvent :

* `< fichier` pour rediriger l'entrée standard depuis le fichier
* `> fichier` pour rediriger l'entrée standard vers le fichier

### exercices ###

1. Placez vous dans le répertoire `redirections`.
2. Utilisez la commande `cat fich1 fich150`.

    Relancez la commande en redirigeant les erreurs dans le fichier
    `erreurs`.   
	Consultez le contenu du fichier `erreurs`. 

3. Ajouter la ligne suivante au fichier `fich2` en utilisant la
   commande `cat` :
   ```
   ligne ajoutée en fin de fichier
   ```

4. Mettez le résultat de la commande `history` dans le fichier
   `mon_travail`.

5. Consultez le contenu du fichier `fich3`. La commande `minuscule`
   (disponible dans le répertoire courant) transforme du texte en
   minuscules. Utilisez cette commande pour convertir le contenu du
   fichier `fich3`. Le résultat de la conversion sera produit dans le
   fichier `fich4`
   
→ Commandes et processus
------------------------

L'exécution d'une commande par le shell donne lieu à un nouveau
processus.  
Par défaut les processus démarrés par le langage de commande le sont
en **avant-plan** : vous ne pouvez exécuter de nouvelles commandes (on
dit que vous n’avez pas la main sur le terminal) tant que le processus
démarré n’est pas terminé.  
Il est possible de démarrer une commande via un processus en
**arrière-plan** (ou tâche de fond). Pour cela il suffit de faire
suivre la commande souhaitée par le caractère « `&` ».

Les langages de commande permettent de démarrer plusieurs commandes en
*séquence* via l’utilisation du caractère de séparation « ; » ou en
*parallèle* via l’utilisation du caractère « & ».

De plus il est possible de grouper le démarrage des processus d’une
suite de commande en les entourant de parenthèses. Par exemple, si un
processus à créer en arrière-plan doit comporter plusieurs commandes
successives il suffit de grouper les commandes avec des parenthèses,
de séparer les commandes souhaitées par des points virgule et de
placer le caractère « & » après la dernière parenthèse fermante comme
dans l’exemple suivant :
```
( commande1 ; commande2 ) &
```

Dans ce cas le shell crée un nouveau processus qui lui même va créer,
en séquence, 2 autres processus, le premier effectuant le code contenu
dans `commande1` puis un second effectuant le code contenu dans
`commande2`.

Chaque processus est repéré par un identifiant système unique
(PID).

La commande `ps` permet de lister les processus en cours
d’exécution sur le système.

La commande `top` permet de monitorer l'état des processus.

Dans certains shells (dont `bash`) il existe une notion connexe aux
identifiants de processus : les identifiants de tâches ou de
**jobs**. Un job est un processus attaché à un terminal en cours
d’utilisation. Dans les lignes de commandes on représentera les jobs
par le caractère « % » suivi du numéro identifiant la tâche (par
ex. `%2`). La commande `jobs` permet de lister les tâches en cours et
leur état. Pour chaque démarrage en arrière-plan le langage de
commande signifie à l’utilisateur le numéro de job, suivi du numéro de
processus (PID).

En bash si un processus est exécuté en avant plan, il est possible de
le suspendre en frappant simultanément sur les touches `Ctrl` et `Z`
. Un processus suspendu peut être redémarré en avant-plan via la
commande `fg` (pour foreground), ou en arrière plan avec la commande
`bg` (pour background).

La commande `kill` permet d’envoyer des
[signaux](https://fr.wikipedia.org/wiki/Signal_(informatique)) aux
processus. Cette commande demande au moins deux paramètres. Le premier
permet de préciser le signal à envoyer au processus. Le second
correspond a l’identification du processus à signaler (via un PID ou
numéro de job). Trois signaux sont particulièrement utile :

+ `KILL` qui demande la destruction sans condition d’un processus
+ `STOP` qui en demande la suspension
+ `CONT` qui demande le redémarrage d’un processus préalablement stoppé.

### exercices ###

1. Démarrez la commande `xeyes`.
2. Essayez maintenant de démarrer une nouvelle fois la commande `xeyes`. Est-ce possible ?
3. Stoppez la commande démarrée en frappant simultanément les touches `Ctrl` et `Z` .
4. Redémarrez la commande stoppée en arrière-plan.
5. Tuez le processus correspondant à `xeyes` en utilisant son numéro
   de job.
6. Trouvez le numéro des processus de `xeyes` et `xcalc` et utilisez
   les pour les stopper au moyen de la commande `kill`. Vérifiez que
   ces processus sont figés.
7. Faites redémarrer le processus `xeyes` au moyen de la commande
   `kill`.
9. Faites redémarrer le processus `xclock` en avant plan au moyen de
   la commande `fg`
10. Débarrassez vous de tous ces processus avec la commande `kill`.

   
→ Filtres
---------

Il est possible de combiner deux commandes en "branchant" la sortie
standard d'une première commande sur l'entrée standard d'une seconde
au moyen d'un **tube**. La syntaxe est la suivante :

```
  commande1 | commande2 | commande3 | ...
```

Les **filtres** sont des commandes qui lisent des données sur leur
entrée standard, les modifient et écrivent le résultat sur leur sortie
standard. Voici les principaux filtres :

commande | effet
--------|-----
`cat` | retourne les lignes lues sans modification.
`cut` | ne retourne que certaines parties de chaque lignes lues.
`grep` | retourne uniquement les lignes lues qui correspondent à un modèle particulier ou qui contiennent un mot précis.
`head` | retourne les premières lignes lues.
`more` | retourne les lignes lues par bloc (dont la taille dépend du nombre de lignes affichables par le terminal) en demandant une confirmation à l’utilisateur entre chaque bloc.
`sed` | édite le texte lu en fonction de commandes.
`sort` | trie les lignes lues.
`tail` | retourne les dernières lignes lues.
`tee` | envoie les données lues sur la sortie standard ET dans un fichier passé en paramètre.
`tr` | remplace des caractères lus par d’autres.
`uniq` | supprime les lignes consécutives identiques.
`wc` | retourne le nombre de caractères, mots et lignes lus.

### exercices ###

1. Placez vous dans le répertoire `filtres`
2. Pour éviter un accident, changez les droits sur le fichier `elus`
   afin que personne ne puisse écrire dedans.
3. Utiliser la commande `wc` pour connaître le nombre de lignes du
   fichier (`man wc` pour savoir comment l'utiliser)
4. Affichez la première ligne du fichier pour avoir une idée du
   contenu.
5. Rechercher les informations concernant votre maire dans le
   fichier (la commande `grep` sera utile).
6. Comptons le nombre de femmes et d'hommes...
   + Tout d'abord nous devons récupérer le champ "code sexe" (commande
     `cut`)
   + Essayez ensuite de voir ce que l'on peut faire avec la commande
     `uniq`.
   + Quelle commande ajouter pour arriver à nos fins ?
   + Bonus 1 : on peut enlever la première ligne inutile ?
   + Bonus 2 : Ça serait plus sympa si on affichait hommes et femmes
     plutôt que H et F (voir la commande `sed`)

→ Variables d'environnement et substitutions
--------------------------------------------

Quand vous tapez une ligne de commande, celle-ci subit un certain
nombre de transformations avant d'être exécutée :

1. Développement des variables : elles sont remplacées par leur valeur
2. substitution de commande : on peut remplacer une commande par sont
   résultat
3. Découpages des mots (espaces, tabulations)
4. Développement des chemins de fichiers

Ces transformations se basent sur des caractères spéciaux qui ont un
sens pour le shell : `espace, tabulation, | & ; ( ), < >, $, ~, ‘, * ?
[], ^, -, {}`

On peut empêcher l'interprétation de ces caractères avec des
protections:
 
+ de caractère avec `\`
  ```
	  $ echo \$HOME
	  $HOME
  ```
  
+ complète avec des quotes `' '`
  ```
	  $ echo '~ et $HOME représentent la même chose'
	  ~ et $HOME représentent la même chose
  ```
  
+ protection simple : les caractères \, $ et `` ` `` sont interprétés
  mais pas les autres
  ```
	  $ echo "~ et $HOME représentent la même chose"
	  ~ et /home/petery représentent la même chose
  ```

Pour remplacer une commande par son résultat (substitution de
commande), on utilise une des syntaxes suivantes : `` `commande` `` ou `$(commande)`


```
		$ echo mon login est $(whoami)
		mon login est petery
```

Rappel : le shell substitue aussi les jokers pour les noms de
fichiers.

### Les variables d'environnement ###

Les variables d'environnement modifient le fonctionnement du shell ou
de certaines commandes. Si vous voulez voir les commandes définie pour
votre shell, utilisez la commande `env`. On peut accéder au contenu
d'une variable avec la syntaxe `${nom_variable}`

Une variable peut être définie par affectation en utilisant la syntaxe
`nom_variable=valeur` (sans espaces).

Une variable définie dans un shell n'est valable que dans ce
shell. Pour qu'elle soit connue des shells fils, il faut utiliser la
commande `export`.

### exercices ###

1. Affichez le contenu de la variable `PWD` avec la commande `echo`.
2. Affichez le contenu de la variable `HOME`, à quoi cela correspond ?
3. La variable `PATH` contient les chemins dans lesquels le shell
   cherche la commande à exécuter. Ces chemins sont séparés par ":"
   + Affichez le contenu de la variable `PATH`
   + Essayez de lancer la commande `salut` qui se trouve dans le
     répertoire `fichier`.
   + Modifiez le `PATH` de manière à pouvoir lancer cette commande de
     n'importe où.
   + Tapez la commande `xterm` pour obtenir un nouveau
     terminal. Placez vous dans celui-ci et taper la commande
     `salut`. Est-ce que cela fonctionne ? Corrigez cela et
     recommencez pour vérifier.

→ (Pour aller plus loin... — Masque des droits)
-----------------------------------------------

Fichiers et répertoires ont des droits de création par défaut. La
commande [`umask`](https://fr.wikipedia.org/wiki/Umask) permet de
visualiser et de modifier ces droits.

1. Visualisez le masque actuellement appliqué. Déduisez en les droits
   obtenus pour les répertoires et les fichiers.
2. Monsieur Optimiste souhaite donner les droits maximum à tout le
   monde sur ses fichiers et répertoires. Modifier le umask en
   conséquence et testez (avec `touch fichier` et `mkdir repertoire`).
3. Monsieur Parano ne veux laisser aucun droit aux membres du groupe et
   aux autres. Modifier le umask en conséquence et testez.

5 — Les scripts
===============

### Valeur de retour d'une commande ###

Une commande renvoie une valeur de retour. Par convention elle renvoie
0 si la commande s'est bien passée et une autre valeur en cas de
problème. Le code de retour de la commande est accessible avec `$?`.
On peut utiliser cela pour combiner deux commandes avec `&&` (ET) et
`||` (OU) :

```
	$ ls /tmp 2> /dev/null && echo le répertoire existe
	le répertoire existe
	$ ls /toto 2> /dev/null && echo le répertoire existe
	
	$ ls /toto 2>/dev/null || echo "le répertoire n'existe pas"
	le répertoire n'existe pas
	$ ls /tmp 2>/dev/null || echo "le répertoire n'existe pas"
```

### Paramètres de position ###

Le nom d'une commande et ses paramètres sont disponibles pour un script shell
avec des noms de variables spécifiques :

+ `$0` correspond au nom de la commande
+ `$1` ... `$9` désigne les différents paramètres de la
  commande
+ `$*` correspond à l'ensemble des paramètres (à partir de `$1`)
+ `$#` donne le nombre de paramètres de la commande

La commande `shift` permet de décaler les paramètres vers la gauche
(i.e., `$1` prend la valeur de `$2`, `$2` prend la valeur de `$3`,
etc.). L'ancienne valeur de `$1` est perdue. Le nombre de paramètres
`$#` est modifié en conséquence.

### Instruction conditionnelle ###

La syntaxe de l'instruction conditionnelle est la suivante :

```
	if <commande>
		then
			...
	elif <commande>
		then
			...
		else
			...
	fi
```

L'instruction `if` (et `elif`) exécute la commande et vérifie sa
valeur de retour. Si la valeur retournée est vraie (0) on rentre dans
le `then` sinon dans le `else`

### Boucles ###

**Boucle `for`** — La syntaxe est la suivante :

```
	for variable in <liste>
	do
		...
	done
```

La boucle `for` itère sur une liste. À chaque itération, la variable
prend la valeur d'un des éléments de la liste. La liste peut être
définie à partir d'une substitution de chemins (méta-caractères) ou de
commande.


**Boucle `while`** — La syntaxe est la suivante :

```
	while <commande>
	do
		...
	done
```

La boucle `while` exécute la commande et vérifie sa valeur de
retour. Si la valeur retournée est vraie (0) on rentre dans la boucle
sinon on passe à la suite.

### (Autres structures de contrôle) ###

Bash offre d'autres structures de contrôle qui sont documentées dans
la page de manuel : `man bash`.

### exercices sur les scripts ###

1. Placez vous dans le répertoire `scripts`
2. Éditez le fichier `mesParametres` et complétez les éléments
   manquants. Testez le.
3. Ecrire un script nommé `existe` qui prend en paramètre un nom de
   fichier. la commande affiche "le fichier existe" si le nom est
   correct et "le fichier n'existe pas" dans la cas contraire. Vous
   utiliserez la commande `ls` pour vérifier l'existence du fichier.
4. Modifiez cette commande pour quelle prenne en paramètre plusieurs
   nom et teste l'existence de chacun des noms.
   
**Commande `test`** — Une commande fréquemment utilisée pour les tests
est la commande `test` qui permet d'effectuer des comparaisons sur des
nombres ou des chaînes de caractères. Elle permet également de
vérifier les propriétés des fichiers et répertoires.

5. Modifiez votre script pour utiliser la commande `test` au lieu de
   `ls`
6. Écrire un script nommé `compteFichiers.sh` qui prend en paramètre
   un ensemble de noms de répertoires.
   + Si aucun paramètre n'est passé, le script affiche le message
     d'erreur "Vous devez passer des noms de répertoires en
     paramètres"
   + Pour chacun des paramètres :
     + Si le nom ne correspond pas à un fichier, le script affiche
       "*nom_fichier* n'existe pas"
	 + Si le nom ne correspond pas à un répertoire, le script  affiche
       le message d'erreur : "*nom_fichier* n'est pas un répertoire"
	 + Si le nom correspond à un répertoire le script affiche :
	   "Le *nom_repertoire* contient *n* fichiers réguliers et *m*
	   sous-répertoires*"
   + S'il y a un problème avec les paramètres (affichage d'un
		message d'erreur), alors la script s'interrompt immédiatement
		et renvoie un résultat faux (différent de 0). Dans le cas
		contraire il renvoie vrai (voir la commande `exit` du shell).

<!-- eof --> 
