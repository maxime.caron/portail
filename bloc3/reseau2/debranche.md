# Activités débranchées autour des protocoles réseaux

L'objectif de cette partie est de comprendre le fonctionnement des couches protocolaires en simulant le fonctionnement du réseau en mode débranché. Pour cela, nous utiliserons des post-it et des enveloppes. Les post-it représentent les différentes adresses ou ports, les enveloppes représentent les couches protocolaires.

## Couche protocolaire TCP/IP/Ethernet avec liaison via un commutateur

Chaque machine est opéré par plusieurs élèves, un par couche. Nous nous limitons à quatre couches (application, transport, réseau, liaison).
Un élève écrit le message HTTP sur un morceau de papier. L'élève suivant insère le papier dans une enveloppe et colle les deux post-it représentant les numéros de port sur l'enveloppe. Le suivant met l'enveloppe dans une autre représentant IP, puis colle deux post-it pour les adresses IP source et destination. L'élève suivant met l'enveloppe dans une troisième enveloppe qui représente le protocole Ethernet, puis colle deux post-it pour les adresses Ethernet source et destination. Enfin, les élèves qui gèrent le commutateur notent les adresses sources et décident sur quel port (quelle rangée) envoyer les enveloppes. Une duplication des enveloppes est éventuellement nécessaire.


## Routage direct entre trois machines

Réaliser un schéma du réseau commuté. Proposer une cheminement du message HTTP "encapsulé" (série d'enveloppes), entre un client HTTP (1er poste) et une serveur HTTP (2nd poste).

Voici un exemple de transmission entre 2 des postes sur le même lien (figure limitée au 3 premières couches) :
![](figs/2_premiers_couches_switch.png)
Quelles décisions sont prises par chacun des équipements intermédiaires et terminaux lors de cette transmission, et à quel niveau (sur quel niveau d'encapsulation, et sur quelles données associées ?)

## Routage de deux réseaux commutés

Réaliser un schéma des réseaux interconnectés par un routeur. Proposer également une cheminement du message HTTP encapsulé, entre un client HTTP (1er poste) et une serveur HTTP (2nd poste sur le second réseau).

Voici un exemple de transmission entre deux postes sur ces deux réseaux respectifs  (figure limitée au 3 premières couches) :
![](figs/3_premiers_couches_switch_router.png)
Quelles décisions sont prises par chacun des équipements intermédiaires et terminaux lors de cette transmission, et à quel niveau (sur quel niveau d'encapsulation, et sur quelles données associées ?)

## Routage de quatre réseaux

On se base sur le schéma suivant (à réaliser lors de la prochaine séance de TP):

![](figs/archi.png)

Tout d'abord, combien y-a t-il de réseaux,  et quelles sont leurs adresses et masques associés respectifs ?

Proposer une cheminement du message HTTP "encapsulé" entre un client HTTP (1er poste) et une serveur HTTP (2nd poste sur un autre réseau).

Au centre de la figure, quelle est ici la différence principale entre le rôle joué par le concentrateur (hub) actuellement représenté. Quelle différence aurait-on s'il était remplacé par un commutateur (switch).
