Groupes
=======

[DIU Enseigner l'informatique au lycée](../Readme.md)
* [promotion 2018/19-2020/21](Readme-1820.md)

Promotion 2020/21-2021/22
-------------------------

* 4 groupes de 16 personnes 
  [2020-22-diu-groupes.csv](2020-22-diu-groupes.csv)
* fiche émargement
  [OpenDocument](2020-22-diu-emargement.ods)
  / [PDF](2020-22-diu-emargement.pdf)
* (pour mémoire, 4 groupes de 12 personnes en janvier 2021)

| nom              | prénom          | groupe |
| ---------------- | --------------- | ------ |
| ACROUTE          | Denis           | A      |
| BAROIS           | David           | C      |
| BEAUCOURT        | Frédéric        | B      |
| BELIN            | Alexis          | B      |
| BOUACHRA         | Bachir          | A      |
| BOURRADA         | Brahim          | B      |
| CARON            | Maxime          | A      |
| CASSEZ           | Olivier         | A      |
| COTTEL           | Thierry         | C      |
| CRITELLI         | Massimo         | C      |
| D AUBUISSON      | Stéphane        | B      |
| DELALLEAU        | Éric            | C      |
| DESESQUELLES     | Francois        | A      |
| DHAUSSY          | Gilles          | A      |
| DJAMAI           | Benaouda        | A      |
| DUCHEMIN         | Frédéric        | A      |
| DUDA             | Aurélie         | A      |
| EUDES            | Fabrice         | A      |
| FALEMPIN         | Émilie Agnès    | A      |
| FROMENTIN        | Arnaud          | A      |
| GALLOT           | Pierre-François | A      |
| GERMAIN          | Philippe        | A      |
| GHESQUIERE       | Cécile          | A      |
| GUILLON          | Gaëtan          | A      |
| LAGODZINSKI      | Rémi            | B      |
| LE MIEUX         | Vincent         | B      |
| LEMAIRE          | Mélanie         | B      |
| LIBBRECHT        | Pierre          | B      |
| LIBERT           | Frédéric        | B      |
| MACKE            | Anthony         | B      |
| MARKEY           | Benoît          | B      |
| MARTIN           | Stephan         | B      |
| MASSIET          | Arnaud          | B      |
| MISLANGHE        | Alexandre       | B      |
| MISSUWE          | Stéphane        | B      |
| MORAGUES         | Arnaud          | B      |
| MOUYS            | Ludovic         | C      |
| NEUTS            | François        | C      |
| NOTREDAME        | Nicolas         | C      |
| PLANQUART        | Patrick         | C      |
| PRZYSZCZYPKOWSKI | Jean-Marc       | C      |
| QUENTON          | Denis           | C      | 
| RAMSTEIN         | Messaline       | C      |
| RITZ             | Florent         | C      |
| ROLLOT           | Frédéric        | C      |
| VANROYEN         | Jean-Philippe   | C      |
| WIATR            | Véronique       | C      |
| WISSART          | Rémi            | C      |
| ACROUTE          | Denis           | A      |
| BAROIS           | David           | C      |
| BEAUCOURT        | Frédéric        | B      |
| BELIN            | Alexis          | B      |
| BOUACHRA         | Bachir          | A      |
| BOURRADA         | Brahim          | B      |
| CARON            | Maxime          | A      |
| CASSEZ           | Olivier         | A      |
| COTTEL           | Thierry         | C      |
| CRITELLI         | Massimo         | C      |
| D AUBUISSON      | Stéphane        | B      |
| DELALLEAU        | Éric            | C      |
| DESESQUELLES     | Francois        | A      |
| DHAUSSY          | Gilles          | A      |
| DJAMAI           | Benaouda        | A      |
| DUCHEMIN         | Frédéric        | A      |
| DUDA             | Aurélie         | A      |
| EUDES            | Fabrice         | A      |
| FALEMPIN         | Émilie Agnès    | A      |
| FROMENTIN        | Arnaud          | A      |
| GALLOT           | Pierre-François | A      |
| GERMAIN          | Philippe        | A      |
| GHESQUIERE       | Cécile          | A      |
| GUILLON          | Gaëtan          | A      |
| LAGODZINSKI      | Rémi            | B      |
| LE MIEUX         | Vincent         | B      |
| LEMAIRE          | Mélanie         | B      |
| LIBBRECHT        | Pierre          | B      |
| LIBERT           | Frédéric        | B      |
| MACKE            | Anthony         | B      |
| MARKEY           | Benoît          | B      |
| MARTIN           | Stephan         | B      |
| MASSIET          | Arnaud          | B      |
| MISLANGHE        | Alexandre       | B      |
| MISSUWE          | Stéphane        | B      |
| MORAGUES         | Arnaud          | B      |
| MOUYS            | Ludovic         | C      |
| NEUTS            | François        | C      |
| NOTREDAME        | Nicolas         | C      |
| PLANQUART        | Patrick         | C      |
| PRZYSZCZYPKOWSKI | Jean-Marc       | C      |
| QUENTON          | Denis           | C      |
| RAMSTEIN         | Messaline       | C      |
| RITZ             | Florent         | C      |
| ROLLOT           | Frédéric        | C      |
| VANROYEN         | Jean-Philippe   | C      |
| WIATR            | Véronique       | C      |
| WISSART          | Rémi            | C      |



<!-- eof --> 
